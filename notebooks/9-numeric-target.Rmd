---
title: "Numeric target"
author: "Alejandro Agustin"
date: "9/1/2019"
output:
  word_document: default
---


```{r message=FALSE, warning=FALSE, include=FALSE, results="hide"}
rm(list=ls())
#setwd("/home/alejandro/code/uni/fib-adei/")
load("../data/8-clustering.RData")
requiredPackages <- c("effects","FactoMineR","car","factoextra","RColorBrewer","ggplot2","dplyr","ggmap","ggthemes","knitr","missMDA","mvoutlier","MVA","mgcv","MASS")
missingPackages <- requiredPackages[!(requiredPackages %in% installed.packages()[,"Package"])]

if(length(missingPackages)) install.packages(missingPackages)
lapply(requiredPackages, require, character.only = TRUE)
library(ggplot2)
library(tidyr)
library(dplyr)
library(cowplot)
library(factoextra)
library(FactoMineR)
```

# IX Model for Numeric Target

## Data preprocess

As detected on previous deliverable, the duration variable is quite skewed. Considering that is our target variable it may generate some problems during the regressions. Thus, we are going to apply a log transformation on every model:


```{r}
pl1 <- ggplot(tbl, aes(x=duration)) + 
  geom_histogram(aes(y=..density..), colour="black", fill="white") +
  geom_density(alpha=.3, fill="#FF6666", color=NA)

pl2 <- ggplot(tbl, aes(x=duration)) + 
  geom_histogram(aes(y=..density..), colour="black", fill="white") +
  geom_density(alpha=.3, fill="#FF6666", color=NA) +
   scale_x_log10()

plot_grid(pl1, pl2, labels = "AUTO")
```

## Model building with continue variables

To start lets only consider numerical variables. 

First we'll consider all the variables and then try to remove those that do not provide anything to the model. Finally, we'll test that the model with all the variables is the same of the cleaned one.



```{r}
# Duration
condes(tbl[,vars_con],2)

# From condes output
m1<-lm(log(duration)~emp.var.rate+cons.conf.idx+euribor3m+nr.employed,data=tbl)
summary(m1)

```
The first model built is composed by the numeric variables that condes found most significant. The R^2 score is very low at 0.006, so I'm going to start with all the variables for the next model.

```{r}
# All numeric variables
m2<-lm(log(duration)~age+campaign+emp.var.rate+cons.price.idx+cons.conf.idx+euribor3m+nr.employed,data=tbl)
summary(m2)
vif(m2)
```

R^2 score is still very low, at 0.12. Anyway the colinearity is high, let's fix that in the next model.

```{r}
m3<-lm(log(duration)~age+campaign+cons.price.idx+cons.conf.idx+nr.employed,data=tbl)
summary(m3)
vif(m3)
```

That looks better. Let's try to further simplify the model.

```{r}
m4<-lm(log(duration)~campaign+cons.price.idx+nr.employed,data=tbl)
summary(m4)
vif(m4)

anova(m3, m4)
```


I don't see how to select better variables at this point.

## Considering non-linear models

For previous models I've supposed that the relation between the explanatory variables ant the target was linear, but that could not be the case.

For each variable I'm going to create a new model with the variable squared and then do a fisher test to see if the models are different.

```{r}
# Campaign
print("CAMPAIGN:")
m5_a<-lm(log(duration)~poly(campaign, 2)+cons.price.idx+nr.employed,data=tbl)
anova(m4, m5_a)

# Cons.price.idx
print("CONS.PRICE.IDX:")
m5_b<-lm(log(duration)~campaign+poly(cons.price.idx, 2)+nr.employed,data=tbl)
anova(m4, m5_b)

# nr.employed
print("NR.EMPLOYED:")
m5_c<-lm(log(duration)~campaign+cons.price.idx+poly(nr.employed, 2),data=tbl)
anova(m4, m5_c)
```
All tests say that higher degree polynomial are not necessary. The p-value for the consumer price index is near 0.05. At the moment the model seems very bad, so I'm going to use it.


```{r}
m6<-lm(log(duration)~campaign+poly(cons.price.idx, 2)+nr.employed,data=tbl)
```

## Considering factors

Let's see how many of those affect our target:

```{r}
# Montact
Boxplot(log(tbl$duration)~tbl$contact,col=heat.colors(2))

# Month
Boxplot(log(tbl$duration)~tbl$month,col=heat.colors(10))

# Season
Boxplot(log(tbl$duration)~tbl$season,col=heat.colors(10))
tapply(log(tbl$duration), tbl$season,summary)

# Day of week
Boxplot(log(tbl$duration)~tbl$day_of_week,col=heat.colors(5))
tapply(log(tbl$duration), tbl$day_of_week,summary)

# Poutcome
Boxplot(log(tbl$duration)~ tbl$poutcome,col=heat.colors(3))

# Housing
Boxplot(log(tbl$duration)~tbl$housing,col=heat.colors(2))

# Education
Boxplot(log(tbl$duration)~tbl$education,col=heat.colors(7))
tapply(log(tbl$duration),tbl$day_of_week,summary)

# Marital
Boxplot(log(tbl$duration)~tbl$marital,col=heat.colors(2))

# Default
Boxplot(log(tbl$duration)~tbl$default,col=heat.colors(2))

# Loan
Boxplot(log(tbl$duration)~tbl$loan,col=heat.colors(2))

# Contact
Boxplot(log(tbl$duration)~tbl$contact,col=heat.colors(2))

# Previous contact
Boxplot(log(tbl$duration)~tbl$f.previous_contacts,col=heat.colors(2))

# Num contacts
Boxplot(log(tbl$duration)~tbl$f.num_contacts,col=heat.colors(2))

```
Visually all the factors seems to have small effect. From all the variables, those that show some variability are: month, poutcome, previous_contacts and num_contacts. Let's add them to the model.

```{r}
m7<-lm(log(duration)~campaign+poly(cons.price.idx, 2)+nr.employed +
         month+poutcome+f.num_contacts+f.previous_contacts,data=tbl)
summary(m7)
```

With those factors an R^2 score of 0.49 is achieved. Let's try to simplify it.

```{r}
step(m7)
m8 <- lm(formula = log(duration) ~ campaign + poly(cons.price.idx, 
    2) + nr.employed + month + poutcome + f.num_contacts, data = tbl)
anova(m7, m8)
anova(m8)
summary(m8)
```

Consumer confidence index does not seem to be very useful, I'm going to drop it from the model.

```{r}
m9 <- lm(formula = log(duration) ~ campaign + nr.employed + month + poutcome + f.num_contacts, data = tbl)

anova(m8, m9)
```

Finally, let's try chaining a variable with their discretization (or the other way) to see if the model improves.

```{r}
m10 <- lm(log(duration) ~ campaign + f.nr.employed + month + poutcome + f.num_contacts, data = tbl)
summary(m10)$r.squared
```
No improvement with number of employers as a factor.

```{r}
m11 <- lm(log(duration) ~ campaign + nr.employed + season + poutcome + f.num_contacts, data = tbl)
summary(m11)$r.squared
```

Neither seems to be beneficial to change the month factor for the season one.

## Final model

The best model I achieved is m8

```{r}
summary(m8)
residualPlots(m8, layout=c(2,2)) 

```

At the *Residuals vs Fited* plot we see what it seems the output of a lineal function with noise. That's a very bad indication, as in a good model it should appear only noise. This mean that there are relations that are not explained by the model, maybe we forget to include a significative variable to our final model.

Finally, at the *Q-Q plot we observe a linear function that bends as it gets to one of the extremes quite heavily. We clearly see the individual data points that are messing with the plot, they could be unprocessed outliers. 



```{r include=FALSE}
save.image("../data/9-numeric-target.RData")
```